atribut:
username
firstname
lastname
address


INSERT / POST
+
1. membuat sebuah biodata dengan seluruh attribut

-
2. membuat biodata tanpa attribut username atau firstname atau address

SELECT / GET
+
1. menampilkan seluruh data
2. menampilkan salah satu id

-
1. menampilkan data yang tidak terdaftar

UPDATE / PUT
+
1. update yang semua atribut
2. update sebagian atribut
3. lastname kosongkan

-
1. salah satu atribut kosong
2. input username yang telah terdaftar

DELETE
+
1. id yang terdaftar

-
1. id yang tidak terdaftar
