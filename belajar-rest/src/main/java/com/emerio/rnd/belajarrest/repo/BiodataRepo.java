package com.emerio.rnd.belajarrest.repo;

import java.util.Optional;

import com.emerio.rnd.belajarrest.entity.Biodata;

import org.springframework.data.repository.CrudRepository;

// ini anaknya bisa ngambil apa yang dari orang tua
// tapi orang tua ga bisa ngambil yg dari anaknya
public interface BiodataRepo extends CrudRepository<Biodata, Long> {
	Optional<Biodata> findByUsername(String name);
}
