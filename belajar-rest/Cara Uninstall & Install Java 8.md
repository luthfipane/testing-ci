# Cara Uninstall & Install Java 8 di Ubuntu

## Uninstall Java
Buka Terminal, ketik :
1. Hapus semua package Java (Sun, Oracle, OpenJDK, IcedTea plugins, GIJ):
```sh
dpkg-query -W -f='${binary:Package}\n' | grep -E -e '^(ia32-)?(sun|oracle)-java' -e '^openjdk-' -e '^icedtea' -e '^(default|gcj)-j(re|dk)' -e '^gcj-(.*)-j(re|dk)' -e '^java-common' | xargs sudo apt-get -y remove
sudo apt-get -y autoremove
```
2. Hapus konfigurasi Java
```sh
dpkg -l | grep ^rc | awk '{print($2)}' | xargs sudo apt-get -y purge
```
3. Hapus direktori Konfigurasi dan Cache Java
```sh
sudo bash -c 'ls -d /home/*/.java' | xargs sudo rm -rf
sudo rm -rf /usr/lib/jvm/*
```
4. Hapus manual JVM
```sh
for g in ControlPanel java java_vm javaws jcontrol jexec keytool mozilla-javaplugin.so orbd pack200 policytool rmid rmiregistry servertool tnameserv unpack200 appletviewer apt extcheck HtmlConverter idlj jar jarsigner javac javadoc javah javap jconsole jdb jhat jinfo jmap jps jrunscript jsadebugd jstack jstat jstatd native2ascii rmic schemagen serialver wsgen wsimport xjc xulrunner-1.9-javaplugin.so; do sudo update-alternatives --remove-all $g; done
```
5. Mencari file/folder Java yang tersisa
```sh
sudo updatedb
sudo locate -b '\pack200'
```

## Install Java 
Buka Terminal, ketik :
1. Tambahkan webupd8team/java pada repository dan update
```sh
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
```
2. Install java8 dan jadikan default
```sh
sudo apt-get install oracle-java8-installer
sudo apt-get install oracle-java8-set-default
```
3. Ketik ini, untuk mengecek versi java
```sh
java -version
```
4. Berikut hasilnya:
```sh
java version "1.8.0_171"
Java(TM) SE Runtime Environment (build 1.8.0_171-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.171-b11, mixed mode)
```